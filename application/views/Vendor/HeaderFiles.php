<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900" rel="stylesheet">
<!-- Simple line Icon -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/simple-line-icons.css">
<!-- Themify Icon -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/themify-icons.css"> -->
<!-- Hover Effects -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/set1.css">
<!-- Main CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom-style.css">