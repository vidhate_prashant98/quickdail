<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Prashant @ DreamKloud Technologies Pvt. Ltd.">
    <meta name="description" content="QuickDial is local search engine.">
    <meta name="keywords" content="#">
    <!-- Page Title -->
    <title>QuickDial &amp; Local search engine</title>
    <?php include("HeaderFiles.php"); ?>
</head>

<body>
    <!-- ============================= HEADER ============================= -->
    <?php include("Header.php");?>
    <!-- SLIDER -->
    <section class="slider d-flex align-items-center">
        <!-- <img src="<?php echo base_url();?>assets/images/slider.jpg" class="img-fluid" alt="#"> -->
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="slider-title_box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="slider-content_wrap">
                                    <h1>Local search engine</h1>
                                    <h5>Let's uncover the best places to eat, drink, and shop nearest to you.</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-10">
                                <form class="form-wrap mt-4">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <input type="text" placeholder="What are your looking for?" class="btn-group1">
                                        <input type="text" placeholder="New york" class="btn-group2">
                                        <button type="submit" class="btn-form"><span class="icon-magnifier search-icon"></span>SEARCH<i class="pe-7s-angle-right"></i></button>
                                    </div>
                                </form>
                                <div class="slider-link">
                                    <a href="#">Browse Popular</a><span>or</span> <a href="#">Recently Added</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// SLIDER -->
    <!--//END HEADER -->
   <b><center> Add vendor page </center></b>
    <form id = "addVendorForm" method="POST" action="<?php echo base_url();?>Vendor/addVendor">
        <div>Name: <input type="text" id="name" name="name" /></div>
        <div>Username: <input type="text" id="username" name="username"/></div>
        <div>Email: <input type="text" id="email" name="email"/></div>
        <div>Mobile: <input type="text" id="mobile" name="mobile"/></div>
        <div>Is Other Vendor: <input type="checkbox" id="isOtherVendor" name="isOtherVendor" value="true"/></div>
        <select name="selectedParentVendor" id="selectedParentVendor">
            <?php if(isset($vendorList)) { foreach($vendorList as $vendor) { ?>
                <option value="<?php echo $vendor->username ?>"><?php echo $vendor->name ?></option>
            <?php } } ?>
        </select>
        <!-- <input type="text" id="" name=""/> -->
        <div><button type="submit">Save</button>
        <button>Reset</button></div>
    </form>
    <?php
     //echo '<pre>';
     //print_r($profileDetail);
    ?>
    <!-- Footer page -->
    <?php include("Footer.php");?>
</body>

</html>
