<!--Template Name: vacayhome
File Name: rooms.html
Author Name: ThemeVault
Author URI: http://www.themevault.net/
License URI: http://www.themevault.net/license/-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("AllJs.php") ?>
    </head>
    <body>
        <div id="page">
            <!--header- -->
            <?php include("Header.php") ?>

            <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                <div class="carousel-inner">
                    <div class="item active"> <img src="<?php echo base_url();?>assets/images/banner/Portfolio/Our-Portfolio.jpg" style="width:100%; height: 500px" alt="First slide">
                    </div>
                </div>
            </div>
            <!--end-->
            <div class="clearfix"></div>

            <!--gallery block- -->
            <section class="gallery-block gallery-front">
                <div class="container">
                    <div class="row">
                        <!-- <div class="navigation">
                            <ul class="nav1 portfolio-filter text-center" style="padding-bottom: 10px;">
                                <li><a class="btn btn-default" href="#" data-filter="*">All</a></li>
                                <li><a class="btn btn-default" data-filter=".web">Web</a></li>
                                <li><a class="btn btn-default" data-filter=".Mobile">Mobile Apps</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".folio">Product</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".logos">SEO</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".mobile">Mobile</a></li>
                                <li><a class="btn btn-default" href="#" data-filter=".mockup">Mockup</a></li>
                            </ul>
                        </div> -->
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 web">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/street 45.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/street 45.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Street 45</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 Mobile">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Flink.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Flink.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a style="margin-left: -23px;">Flink Streaming</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/prego_cafe.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/prego_cafe.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Prego cafe</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/OrderOnChef.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/OrderOnChef.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Order on chef</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 web">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/drooosi.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/drooosi.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Droosi</a></p>
                                </div>
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 Mobile">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/guide_tips.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/guide_tips.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Guide tips</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/shawarma.png">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/shawarma.png" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Shawarma</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/myroommate.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/myroommate.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>My Roommate</a></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 web">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Website_high_quality.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Website_high_quality.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Website high quality</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 Mobile">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Live_KH.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Live_KH.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Live KH</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/digital360.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/digital360.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Digital 360</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Website_evonix.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Website_evonix.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Website Evonix</a></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 web">
                            <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/readyforshop.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/readyforshop.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a style="margin-left: -23px;">Ready For Shop</a></p>
                                </div>
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 Mobile">
                            <div class="gallery-image Mobile">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Website_md_me.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Website_md_me.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Website md me</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <!-- <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Website_high_quality.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/Website_high_quality.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>Website high quality</a></p>
                                </div>
                            </div> -->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <!-- <div class="gallery-image web">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/myroommate.jpg">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/portfolio/myroommate.jpg" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>My Roommate</a></p>
                                </div>
                            </div> -->
                        </div> 
                    </div>
                </div>
            </section>

            <!--service block- --
            <section class="service-block">
                <div class="bg-set-col">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                                <div class="service-details text-center">
                                    <div class="service-image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/icons/wifi.png">
                                    </div>
                                    <h4><a>free wifi</a></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                                <div class="service-details text-center">
                                    <div class="service-image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/icons/key.png">
                                    </div>
                                    <h4><a>room service</a></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                                <div class="service-details text-center">
                                    <div class="service-image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/icons/car.png">
                                    </div>
                                    <h4><a>free parking</a></h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                                <div class="service-details text-center">
                                    <div class="service-image">
                                        <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/icons/user.png">
                                    </div>
                                    <h4><a>customer support</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!---footer -->
            <?php include("footer.html") ?>

            <!--back to top- -->
            <a style="display: none;" href="javascript:void(0);" class="scrollTop back-to-top" id="back-to-top">
                <span><i aria-hidden="true" class="fa fa-angle-up fa-lg"></i></span>
                <span>Top</span>
            </a>

        </div>
    </body>
</html>
