<link rel="icon" href="<?php echo base_url();?>assets/images/Logo/Logo_Final_trans.png"/>
<title>DreamKloud</title>
<!-- Bootstrap core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Custom styles for this template -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/fonts/antonio-exotic/stylesheet.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lightbox.min.css">
<link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/lightbox-plus-jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/instafeed.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/custom.js" type="text/javascript"></script>