<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

<!--Template Name: vacayhome
File Name: home.html
Author Name: ThemeVault
Author URI: http://www.themevault.net/
License URI: http://www.themevault.net/license/-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("AllJs.php") ?>
    </head>
    <body>
        <div id="page">
            <?php include("Header.php") ?>
            <!--end-->
            <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                <!-- Indicators -->

                <ol class="carousel-indicators">
                    <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel1" data-slide-to="1"></li>
                    <li data-target="#myCarousel1" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active"> <img src="<?php echo base_url();?>assets/images/banner/Home/banner_1.jpg" style="width:100%; height: 500px" alt="First slide">
                        <div class="carousel-caption">
                            <h1>Welcome to <br> DreamKloud Technologies</h1>
                        </div>
                    </div>
                    <div class="item"> <img src="<?php echo base_url();?>assets/images/banner/Home/banner_2.jpg" style="width:100%; height: 500px" alt="Second slide">
                        <div class="carousel-caption">
                            <h1>MEET THE BUSINESS NEEDS WITH<br> DreamKloud Technologies</h1>
                        </div>
                    </div>
                    <div class="item"> <img src="<?php echo base_url();?>assets/images/banner/Home/banner_3.jpg" style="width:100%; height: 500px" alt="Third slide">
                        <div class="carousel-caption">
                            <h1>TOGETHER BUILDING THE <br> FUTURE</h1>
                        </div>
                    </div>

                </div>
                <a class="left carousel-control" href="#myCarousel1" data-slide="prev"> <img src="<?php echo base_url();?>assets/images/icons/left-arrow.png" onmouseover="this.src = '<?php echo base_url();?>assets/images/icons/left-arrow-hover.png'" onmouseout="this.src = '<?php echo base_url();?>assets/images/icons/left-arrow.png'" alt="left"></a>
                <a class="right carousel-control" href="#myCarousel1" data-slide="next"><img src="<?php echo base_url();?>assets/images/icons/right-arrow.png" onmouseover="this.src = '<?php echo base_url();?>assets/images/icons/right-arrow-hover.png'" onmouseout="this.src = '<?php echo base_url();?>assets/images/icons/right-arrow.png'" alt="left"></a>

            </div>
            <div class="clearfix"></div>

            <!--service block- -->
            <section class="service-block" id="services-block">
                <div class="container">
                    <div class="product-desc-side" style="height: 0px !important;padding: 0px;">
                        <h3>Services</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-globe fa-stack-1x"></i>
                                </span>
                                <h4><a>Web Development</a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-mobile fa-stack-1x"></i>
                                </span>
                                <h4><a>Mobile Apps Development</a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-shopping-cart fa-stack-1x"></i>
                                </span>
                                <h4><a>CRM / ERP Application</a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-search fa-stack-1x"></i>
                                </span>
                                <h4><a>Digital marketing & SEO</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-cloud fa-stack-1x"></i>
                                </span>
                                <h4><a>Web Hosting </a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 width-50">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-bar-chart fa-stack-1x"></i>
                                </span>
                                <h4><a>Report Solution</a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-desktop fa-stack-1x"></i>
                                </span>
                                <h4><a> Desktop App Development</a></h4>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 mt-25">
                            <div class="service-details text-center top-margin">
                                <span class="fa-stack fa-4x">
                                    <i class="fa fa-circle fa-stack-2x" style="color: #ff8795a1;"></i>
                                    <i class="fa fa-circle-thin fa-stack-2x" style="color: #ff4157;"></i>
                                    <i class="fa fa-wifi fa-stack-1x"></i>
                                </span>
                                <h4><a>IOT Development</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--gallery block- --
            <section class="gallery-block gallery-front">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/room1.png">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/room1.png" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>delux room</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/room2.png">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/room2.png" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>super room</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/room3.png">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/room3.png" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>single room</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="gallery-image">
                                <img class="img-responsive" src="<?php echo base_url();?>assets/images/room4.png">
                                <div class="overlay">
                                    <a class="info pop example-image-link img-responsive" href="<?php echo base_url();?>assets/images/room4.png" data-lightbox="example-1"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <p><a>double room</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--offer block--
            <section class="vacation-offer-block">
                <div class="vacation-offer-bgbanner">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-6 col-xs-12">
                                <div class="vacation-offer-details">
                                    <h1>Your vacation Awaits</h1>
                                    <h4>Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit.</h4>
                                    <button type="button" class="btn btn-default">Choose a package</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End-->

            <!----resort-overview- -->
            <section class="resort-overview-block" id="projects-block">
                <div class="container">
                    <div class="product-desc-side" style="height: 50px !important;padding: 0px;">
                        <h3>Top Projects</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 remove-padd-right">
                            <div class="side-A">
                                <div class="product-thumb">
                                    <div class="image">
                                        <a><img src="<?php echo base_url();?>assets/images/portfolio/digital360.jpg" class="img-responsive" alt="image"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="side-B">
                                <div class="product-desc-side">
                                    <h3><a>Digital 360</a></h3>
                                    <p>This <b>website</b> is for digital Marketing/Branding. In this they offers complete Digital Branding solution and gives 360&deg online marketing solution for any business.</p>
                                    <div class="links"><a href="<?php echo base_url();?>/Home/ProgressPage">Read more</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12 remove-padd-left">
                            <div class="side-A">
                                <div class="product-thumb">
                                    <div class="image">
                                        <a><img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/portfolio/Flink.jpg"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="side-B">
                                <div class="product-desc-side">
                                    <h3><a>Flink Streaming</a></h3>
                                    <p>In this project we are implemented the real-time data processing using <b>flink-streaming</b> on any relation database.</p>
                                    <div class="links"><a href="<?php echo base_url();?>/Home/ProgressPage">Read more</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12 remove-padd-right">
                            <div class="side-A">
                                <div class="product-desc-side">
                                    <h3><a>Prego Cafe</a></h3>
                                    <p>Multi-Vendor online Shopping order <b>ecommerce mobile application</b> like amazon, filpkart. </p>
                                    <div class="links"><a href="<?php echo base_url();?>/Home/ProgressPage">Read more</a></div>
                                </div>
                            </div>

                            <div class="side-B">
                                <div class="product-thumb">
                                    <div class="image txt-rgt">
                                        <a class="arrow-left"><img src="<?php echo base_url();?>assets/images/portfolio/prego_cafe.jpg" class="img-responsive" alt="imaga"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6 col-sm-12 col-xs-12 remove-padd-left">
                            <div class="side-A">
                                <div class="product-desc-side">
                                    <h3><a>Ready for shop</a></h3>
                                    <p>This is <b>ecommerce website</b> for Mobiles,Accessories, books, Men's apparels, Herbal products, Pets care with free shipping & cash on delivery.</p>
                                    <div class="links"><a href="<?php echo base_url();?>/Home/ProgressPage">Read more</a></div>
                                </div>
                            </div>

                            <div class="side-B">
                                <div class="product-thumb txt-rgt">
                                    <div class="image">
                                        <a class="arrow-left"><img src="<?php echo base_url();?>assets/images/portfolio/readyforshop.jpg" class="img-responsive" alt="imaga"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </section>

            <!-----blog slider--- -->
            <!--blog trainer block--
            <section class="blog-block-slider">
                <div class="blog-block-slider-fix-image">
                    <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                        <div class="container">
                            <div class="product-desc-side" style="height: 50px !important;padding: 0px;">
                                <h3>Clients Review</h3>
                            </div>
                            <!-- Wrapper for slides --
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel2" data-slide-to="1"></li>
                                <li data-target="#myCarousel2" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <div class="blog-box">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only</p>
                                    </div>
                                    <div class="blog-view-box">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="<?php echo base_url();?>assets/images/client1.png" class="media-object">
                                            </div>
                                            <div class="media-body">
                                                <h3 class="media-heading blog-title">Walter Hucko</h3>
                                                <h5 class="blog-rev">Satisfied Customer</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="blog-box">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only</p>
                                    </div>
                                    <div class="blog-view-box">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="<?php echo base_url();?>assets/images/client2.png" class="media-object">
                                            </div>
                                            <div class="media-body">
                                                <h3 class="media-heading blog-title">Jules Boutin</h3>
                                                <h5 class="blog-rev">Satisfied Customer</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="blog-box">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only</p>
                                    </div>
                                    <div class="blog-view-box">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="<?php echo base_url();?>assets/images/client3.png" class="media-object">
                                            </div>
                                            <div class="media-body">
                                                <h3 class="media-heading blog-title">Attilio Marzi</h3>
                                                <h5 class="blog-rev">Satisfied Customer</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>

            <!---blog block- --
            <section class="blog-block">
                <div class="container">
                     <div class="product-desc-side" style="height: 15px !important;padding: 0px;">
                        <h3>Latest Blogs</h3>
                    </div>
                    <div class="row offspace-45">
                        <div class="view-set-block">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="event-blog-image">
                                    <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/blog1.png">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 side-in-image">
                                <div class="event-blog-details">
                                    <h4><a href="single-blog.html">Lorem ipsum dolor sit amet</a></h4>
                                    <!--<h5>Post By Admin <a><i aria-hidden="true" class="fa fa-heart-o fa-lg"></i>Likes</a><a><i aria-hidden="true" class="fa fa-comment-o fa-lg"></i>comments</a></h5>--
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lorem nulla, ornare eu felis quis, efficitur posuere nulla. Aliquam ac luctus turpis, non faucibus sem. Fusce ornare turpis neque, eu commodo sapien porta sed. Nam ut ante turpis. Nam arcu odio, scelerisque a vehicula vitae, auctor sit amet lectus. </p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lorem nulla, ornare eu felis quis, efficitur posuere nulla. Aliquam ac luctus turpis, non faucibus sem. Fusce ornard hendrerit tortor vulputate id. Vestibulum mauris nibh, luctus non maximus vitae, porttitor eget neque. Donec tristique nunc facilisis, dapibus libero ac</p>
                                    <a class="btn btn-default" href="single-blog.html">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row offspace-45">
                        <div class="view-set-block">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="event-blog-image">
                                    <img alt="image" class="img-responsive" src="<?php echo base_url();?>assets/images/blog2.png">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12 side-in-image">
                                <div class="event-blog-details">
                                    <h4><a href="single-blog.html">Lorem ipsum dolor sit amet</a></h4>
                                    <!--<h5>Post By Admin <a><i aria-hidden="true" class="fa fa-heart-o fa-lg"></i>Likes</a><a><i aria-hidden="true" class="fa fa-comment-o fa-lg"></i>comments</a></h5>--
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lorem nulla, ornare eu felis quis, efficitur posuere nulla. Aliquam ac luctus turpis, non faucibus sem. Fusce ornare turpis neque, eu commodo sapien porta sed. Nam ut ante turpis. Nam arcu odio, scelerisque a vehicula vitae, auctor sit amet lectus. </p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lorem nulla, ornare eu felis quis, efficitur posuere nulla. Aliquam ac luctus turpis, non faucibus sem. Fusce ornard hendrerit tortor vulputate id. Vestibulum mauris nibh, luctus non maximus vitae, porttitor eget neque. Donec tristique nunc facilisis, dapibus libero ac</p>
                                    <a class="btn btn-default" href="single-blog.html">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!---footer- -->
            <?php include("footer.html") ?>

            <!--back to top- -->
            <a style="display: none;" href="javascript:void(0);" class="scrollTop back-to-top" id="back-to-top">
                <span><i aria-hidden="true" class="fa fa-angle-up fa-lg"></i></span>
                <span>Top</span>
            </a>

        </div>
    </body>
</html>
