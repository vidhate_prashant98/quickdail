<!--Template Name: vacayhome
File Name: about.html
Author Name: ThemeVault
Author URI: http://www.themevault.net/
License URI: http://www.themevault.net/license/-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("AllJs.php") ?>
    </head>
    <body>
        <div id="page">
            <?php include("Header.php") ?>
            <!--end-->
            <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                <div class="carousel-inner">
                    <div class="item active"> <img src="<?php echo base_url();?>assets/images/banner/About/About_us.jpg" style="width:100%; height: 500px" alt="First slide">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <section class="about-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 about-left">
                            <p>Build your<label> Dream</label> With  <span>-us</span></p>
                        </div>
                        <div class="col-md-7 about-right ">
                            <span class="event-blog-details">
                            <h3>Our Mission</h3>
                                <p>To excel at helping our customers worldwide achieve and transcend their business/professional goals by providing quality web and 
                                software solutions. Increase the assets and investments of the company, to support the development of services.</p>
                            </span>
                            <span class="event-blog-details">
                            <h3>Our Values</h3>
                            <ul class="list-unstyled list-inline">
                                <li>In all our assignments, we abide by some core values:Honesty and IntegrityCustomer Satisfaction and Delight.</li>
                                <li>We believe in treating our customers with respect and faith.</li>
                                <li>We grow through creativity, invention and innovation.</li>
                                <li>We integrate honesty, integrity and business ethics into all aspects of our business functioning.</li>
                            <ul>
                            </span>

                            <span class="event-blog-details">
                            <h3>About us</h3>
                                <p> DreamKloud Technology is Outsourcing product development to companies in India offers significant cost advantage. 
                                    There are several companies in India who offer such services. Expert team to find solutions for your web development needs</p>
                                <p>Exceeding our customers expectations is fundamental to each service we deliver. DreamKloud Technology delivers the most effective IT solutions 
                                    available through a mature but dynamic model that recognizes and responds to changes in technology, markets, and each client’s 
                                    business objectives.</p>
                                <p>Our professional helps in creating interactive Websites and Software Applications, Multimedia Presentations by latest technology.</p>
                            </span>
                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit</span>-->
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </section>

            <!---footer- -->
            <?php include("footer.html") ?>

            <!--back to top- -->
            <a style="display: none;" href="javascript:void(0);" class="scrollTop back-to-top" id="back-to-top">
                <span><i aria-hidden="true" class="fa fa-angle-up fa-lg"></i></span>
                <span>Top</span>
            </a>

        </div>
    </body>
</html>
