<!--Template Name: vacayhome
File Name: contact.html
Author Name: ThemeVault
Author URI: http://www.themevault.net/
License URI: http://www.themevault.net/license/-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("AllJs.php") ?>
    </head>
    <body>
        <div id="page">
            <!---header top-- -->
            <?php include("Header.php") ?>
            <div id="myCarousel1" class="carousel slide" data-ride="carousel"> 
                <div class="carousel-inner">
                    <div class="item active"> <img src="<?php echo base_url();?>assets/images/banner/Contact/contact_us.jpg" style="width:100%; height: 500px" alt="First slide">
                        <div class="carousel-caption">
                            <h1>Contact us</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!--end-->
            <div class="clearfix"></div>


            <section class="contact-block">
                <div class="container">
                    <div class="col-md-6 contact-left-block">
                        <h3><span>Contact </span>Us</h3>
                        <p class="text-left">Please contact us for more details.</p>
                        <p class="text-right">Opp of Dairy Don, College Road, Nashik<i class="fa fa-map-marker fa-lg"></i></p>
                        <p class="text-right"><a href="tel:+1-202-555-0100"> 8600877991, 8805735704 <i class="fa fa-phone fa-lg"></i></a></p>
                        <p class="text-right"><a href="mailto:demo@info.com"> info@dreamkloud.in <i class="fa fa-envelope"></i></a></p>
                    </div>
                    <div class="col-md-6 contact-form">
                        <h3>Send a <span>Message</span></h3>
                        <form action="#" method="post">
                            <input type="text" class="form-control" id="contactName" name="contactName" placeholder="Name" required="">
                            <input type="email" class="form-control" name="contactEmail" id="contactEmail" placeholder="Email" required="">
                            <textarea class="form-control" name="contactMessage" id="contactMessage" placeholder="Message Here...." required=""></textarea>
                            <input type="Button" class="submit-btn" value="Submit" onclick="submitContactUs()">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </section>

            <!---map- -->
            <section class="offspace-70">
                <div class="map">
                    <div class="container">
                        <iframe src="https://www.google.com/maps/embed/v1/place?q=Kusum%20Pushpa%20Apt%2C%20Opp%20of%20Dairy%20Don%2C%20College%20Road%2C%20Nashik&key=AIzaSyB9lgIyulH3KTss_KYcp3EIvjOOzQf3Jew"  frameborder="0" style="border:0; width: 100%; height: 400px" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
            <?php include("footer.html") ?>

            <!--back to top- -->
            <a style="display: none;" href="javascript:void(0);" class="scrollTop back-to-top" id="back-to-top">
                <span><i aria-hidden="true" class="fa fa-angle-up fa-lg"></i></span>
                <span>Top</span>
            </a>

        </div>

        <script>
            function submitContactUs(){
                var name = $("#contactName").val();
                var email = $("#contactEmail").val();
                var message = $("#contactMessage").val();
                var data={'name':name,'email':email,'message':message};
                console.log(data);

                $.ajax({
                    type:"POST",
                    url: '<?php echo base_url();?>Home/SubmitContactUs',
                    data: data,
                    beforeSend: function(){ 
                        $("#contact-success").html('<div class="alert alert-success"><p class="text-success">Contacting us please wait</p></div>').fadeIn();
                    },
                    success:function(data){
                        $("#contact-success").html('<div class="alert alert-success"><p class="text-success">Thank you for contact us. We will contact you early as possible.</p></div>').delay(3000).fadeOut();
                        $('#contactName').val("");
                        $("#contactEmail").val("");
                        $("#contactMessage").val("");
                    }
                });
            }
        </script>
    </body>
</html>
