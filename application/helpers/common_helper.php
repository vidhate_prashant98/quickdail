<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
function uploadFile($img,$elementName='file')
    {
		$CI =& get_instance();
		$dt = '';
      $ext = pathinfo($img, PATHINFO_EXTENSION);
		$dt = time().uniqid();
        $filename = $dt.'.'.$ext;
        $tileFileName = 'tile_'.$dt.'.'.$ext;
		$fn = $elementName;	
        if (!is_dir('uploads'))
        {
            mkdir('./uploads', 0777, TRUE);
        }
        $user_folder = 'excelfile'; // User folder name
        if (!is_dir('uploads/'.$user_folder))
        {
            mkdir('./uploads/'.$user_folder, 0777, TRUE);
        }
        $destination_image_path = './uploads/'.$user_folder.'/';
		//$destination_image_path = './uploads/';
        $config['upload_path'] = $destination_image_path;
        $config['allowed_types'] = 'csv';
        $config['max_size']    = '1000000';
        $config['max_width']  = '10024';
        $config['max_height']  = '7068';
        $config['file_name'] = $filename;
        $CI->load->library('upload', $config);
        $CI->upload->initialize($config);
        if ( ! $CI->upload->do_upload($fn))
        {
           $error = array('error' => $CI->upload->display_errors());
            //echo '<pre>';
			/*print_r($error); 
            exit;*/

            //echo 502; exit;
        }
        else
        {
            $inputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$filename;
			$outputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$tileFileName;
			//if(imgResize($inputFile,$outputFile))
				return $filename;
        }
    }
    
    function uploadImage($img,$elementName='image')
    {
		$CI =& get_instance();
		$dt = '';
       
       $scholar_id = $CI->session->userdata('scholarId');
      $ext = pathinfo($img, PATHINFO_EXTENSION);
		$dt = time().uniqid();
        $filename = $dt.'.'.$ext;
        $tileFileName = 'tile_'.$dt.'.'.$ext;
			 $fn = $elementName;		
         if (!is_dir('uploads'))
        {
            mkdir('./uploads', 0777, TRUE);
        }
       // $user_folder = 'imagefile'; // User folder name
       $scholar_folder = $scholar_id; // User folder name
        if (!is_dir('uploads/'.$scholar_folder))
        {
            mkdir('./uploads/'.$scholar_folder, 0777, TRUE);
            //chmod('./uploads/'.$scholar_folder, 0777,'r');
            //fopen('./uploads/'.$scholar_folder, 'r');
            $destination_image_path = './uploads/'.$scholar_folder;
        }
        else{
        		$destination_image_path = './uploads/'.$scholar_folder;
        }

		//$destination_image_path = './user/';
        $config['upload_path'] = $destination_image_path.'/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']    = '2097152';
        $config['max_width']  = '10024';
        $config['max_height']  = '7068';
        $config['file_name'] = $filename;
        $CI->load->library('upload', $config);

        $CI->upload->initialize($config);

        if ( ! $CI->upload->do_upload($fn))
        {
           $error = array('error' => $CI->upload->display_errors());

			//echo "%%".$fn."%%";

            //echo '<pre>';
          //  return $error;
			//var_dump($error);
			//exit;

            //echo 502; exit;
            //return $error;

        }
        else
        {
            $inputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$filename;
				$outputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$tileFileName;

			//if(imgResize($inputFile,$outputFile))
				return $filename;
        }
    }
 ?> 