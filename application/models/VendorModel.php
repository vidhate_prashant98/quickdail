<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VendorModel extends CI_Model 
{  
	function __CONSTRUCT() {

    }

    function getVendorListByUsername($username) {
        $query = $this->db->query("SELECT user.* FROM vendor_mapping INNER join user ON user.username = vendor_mapping.child_vendor where vendor_mapping.parent_vendor = '$username'");
		if($query->num_rows()>0) {
			return $query->result();		
		} else {
			return null;
		}
	}

	function AddVendor($name, $username, $email, $mobile, $parentVendorUsername) {
		$this->addVendorToUser($name, $username, $email, $mobile); //add to user table
		echo 'user added';
		$this->addVendorToLogin($username);
		echo 'user added in LOGIN';
		$this->addVendorRoleMapping($username);  //add to user role
		echo 'user added in USER_ROLE';
		$this->addVendorMapping($username, $parentVendorUsername);
		echo 'user added in VENDOR_MAPPING';
		$this->addVendorWidth($username);	//add entry to row level width
		echo 'user added in WIDTH';
		$this->addVendorHeight($username);	//add entry to height 
		echo 'user added in HEIGHT';
		$this->incrementVendorWidthByone($parentVendorUsername);	//increment row width by one of parent vendor
		echo 'user added in WIDTH BY 1';
		$this->incrementVendorHeightByone($parentVendorUsername);	//increment height by one of parent vendor if width is 1
		echo 'user added in HEIGHT BY 1';
		return true;
	}

	function addVendorToLogin($username) {
		$data = array(
			'username' => $username,
			'password' => date('Y-m-d h:i:s'),
			'created_at' => date('Y-m-d h:i:s')
		);
		
		if($this->db->insert('login',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function addVendorWidth($username) {
		$data = array(
			'username' => $username,
			'levelcount' => '0'
		);

		if($this->db->insert('user_level_width',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function addVendorHeight($username) {
		$data = array(
			'username' => $username,
			'levelcount' => '0'
		);

		if($this->db->insert('user_level_height',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function incrementVendorWidthByone($parentVendorUsername) {
		$levelWidth = 0;
		$data = $this->db->query("SELECT  * FROM user_level_width where username = '$parentVendorUsername'");
		if($data->num_rows() > 0) {
			$levelWidth = $data->row()->levelcount;
			$levelWidth++;

			$updatedData = array(
				'username' => $parentVendorUsername,
				'levelcount' => $levelWidth
			);
			
			$this->db->where('username', $parentVendorUsername);
			$this->db->update('user_level_width', $updatedData);
		} else {
			//add new entry set count to 1 :: Low priority
			$updatedData = array(
				'username' => $parentVendorUsername,
				'levelcount' => 1
			);

			$this->db->insert('user_level_width',$updatedData);		
		}
	}

	function incrementVendorHeightByone($parentVendorUsername) {
		$levelHeight = 0;

		$data = $this->db->query("SELECT  * FROM user_level_width where username = '$parentVendorUsername'");
		$levelWidth = $data->row()->levelcount;

		if($levelWidth == 1) {
			$heightResult = $this->db->query("SELECT  * FROM user_level_height where username = '$parentVendorUsername'");
			if($heightResult->num_rows() > 0){
				$levelHeight = $heightResult->row()->levelcount;
				$levelHeight++;

				$updatedData = array(
					'username' => $parentVendorUsername,
					'levelcount' => $levelHeight
				);
				
				$this->db->where('username', $parentVendorUsername);
				$this->db->update('user_level_height', $updatedData);
			} else {
				$levelHeight = 1;

				$updatedData = array(
					'username' => $parentVendorUsername,
					'levelcount' => $levelHeight
				);
	
				$this->db->insert('user_level_height', $updatedData);		
			}
		}
	}

	function addVendorRoleMapping($username) {
		$data = array(
			'username' => $username,
			'role' => 'VENDOR',
			'created_date' => date('Y-m-d h:i:s')
		);
		
		if($this->db->insert('user_role',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function addVendorMapping($username, $parentVendorUsername) {
		$data = array(
			'child_vendor' => $username,
			'parent_vendor' => $parentVendorUsername
		);
		
		if($this->db->insert('vendor_mapping',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function addVendorToUser($name, $username, $email, $mobile) {
		$data = array(
			'username' => $username,
			'name' =>$name,
			'mobile' =>$mobile,
			'email'=>$email,
			'deleted' =>"false",
            'created_date'   =>  date('Y-m-d h:i:s')
		);
		
		if($this->db->insert('user',$data)) {
			$user_id = $this->db->insert_id();
			return true;
		}

		return false;
	}

	function getVendorWidth($parentVendorUsername) {
		$data = $this->db->query("SELECT  * FROM user_level_width where username = '$parentVendorUsername'");
		if($data->num_rows() > 0) {
			return $data->row();
		} 
		return null;
	}

	function getVendorHeight($parentVendorUsername) {
		$data = $this->db->query("SELECT  * FROM user_level_height where username = '$parentVendorUsername'");
		if($data->num_rows() > 0) {
			return $data->row();
		} 
		return null;
	}
}
