<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CommonModel extends CI_Model 
{  
	function __CONSTRUCT() {

    }

    function getProfile($username) {
		$query = $this->db->query("SELECT user.* FROM login as login INNER JOIN user as user ON user.username = login.username INNER JOIN user_role as role ON role.usrename = login.username where login.username = '$username'");
		if($query->num_rows()>0) {
			return $query->row();		
		} else {
			return null;		
		}
	}
	
	function getCategories() {
		$query = $this->db->query("SELECT * FROM categories");
		if($query->num_rows()>0) {
			return $query->result();		
		} else {
			return null;		
		}
	}

}