<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation','upload','email'));
        $this->load->helper(array('url','form','common_helper','text','string'));
        $this->load->model('AdminModel');
        $this->load->model('CommonModel');
        if($this->session->userdata('role') != 'ADMIN') {
            redirect(base_url().'Home/HomePage');
        }
    }
        
    public function logout()
    {
        $this->session->unset_userdata("admin_username");
        $this->session->unset_userdata("admin_role");
        redirect('Home/HomePage');
    }

    function viewProfile() {
        $username = $this->session->userdata('admin_username'); 
        $data['profileDetail'] = $this->CommonModel->getProfile($username);
        $this->load->view('Admin/Profile',$data);
    } 
    function updateAdmin()
    {
        $name = htmlentities($this->security->xss_clean($_POST['adminName']));
        $username = htmlentities($this->security->xss_clean($_POST['username']));
        $email = htmlentities($this->security->xss_clean($_POST['email']));
        $id = $this->AdminModel->updateAdmin($name,$username,$email);
        if($id)
        {
            $this->session->set_flashdata('success', 'Your profile is updated successfully');
            redirect('Admin/myAccount');
        }
        else
        {
            $this->session->set_flashdata('Error', 'Their is some problem in updation');
            redirect('Admin/myAccount');
        }
    }
    function changePassword()
    {
        $this->load->view('admin/changepassword_view');
    }
    function updatePassword()
    {
        $oldpassword = sha1(htmlentities($this->security->xss_clean($this->input->post('oldpassword'))));
		$newpassword = sha1(htmlentities($this->security->xss_clean($this->input->post('newpassword'))));
        if($this->AdminModel->updatePassword($oldpassword,$newpassword))
        {
                $this->session->set_flashdata('success', 'Password has been updated successfully. Please Login to continue');
                redirect('Admin/changePassword');
                // $this->logout();
                // redirect('Home/view_login');	
        }
        else 
        {
                $this->session->set_flashdata('error','Old password do not matched');
                redirect('Admin/changePassword');	
        }	
    }
}
?>
