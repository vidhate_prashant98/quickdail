<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation','upload','email'));
        $this->load->helper(array('url','form','common_helper','text','string'));
        $this->load->model('AdminModel', 'HomeModel');
        if(!$this->session->userdata('role')) {
            redirect(base_url().'Home/HomePage');
        }
    }
}