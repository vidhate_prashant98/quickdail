<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	function __construct() {
        parent::__construct();
        $this->load->helper(array('url','form','string'));
        $this->load->library(array("session","form_validation","email"));
        $this->load->model('homeModel');
        $this->load->model('commonModel');
    }

	public function HomePage() {
        $this->commonModel->getCategories();
        $this->load->view('Home/Homepage');
    }

    public function Login() {
        $this->load->view('Home/LoginPage');
    }

    public function AdminDashboard() {
        $this->load->view('Admin/AdminDashboard');
    }

    public function VendorDashboard() {
        $this->load->view('Vendor/VendorDashboard');
    }

    public function Authenticate() {
        $uname = htmlentities($this->security->xss_clean($this->input->post('username')));
        $pwd =sha1($this->input->post('password'));
        $loginInfo = $this->homeModel->login($uname,$pwd);
        if($loginInfo){
            $this->session->set_userdata("role", $loginInfo->role);
            
            $username = $loginInfo->username;
            $role = $loginInfo->role;

            if($role == 'ADMIN'){
                $this->session->set_userdata("admin_username", $loginInfo->username);
                $this->session->set_userdata("admin_role", $loginInfo->role);
   
                redirect('home/AdminDashboard');
            }

            if($role == 'VENDOR'){
                $this->session->set_userdata("vendor_username", $loginInfo->username);
                $this->session->set_userdata("vendor_role", $loginInfo->role);

                redirect('home/VendorDashboard');
            }
        } else {
			redirect('home/Login');							
		}
    }

    public function ContactUs() {
        $this->load->view('User/ContactUs');
    }

    public function ProgressPage() {
        $this->load->view('Progress');
    }

    public function SubmitContactUs() {
        $name = $_POST['name'];
        $message = $_POST['message'];
        $email = $_POST['email'];
        
        // Create the email and send the message
        $to = 'info@dreamkloud.in'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
        $email_subject = "Enquiry from :  $name";
        $email_body = "<strong>Hi Prashant/Mahesh,</strong> <br> <p>This is enquiry Email from user. </p> <p> User details are below :</p> 
            <p>Name : $name</p> 
            <p>Email : $email</p> 
            <p>Detailed message : $message</p> 
            <strong>Regards,</strong><br><strong>Suport team</strong>";        
        $headers = "From: info@dreamkloud.in.ar\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
        $headers .= "Reply-To: $email";   
        $result = mail($to,$email_subject,$email_body,$headers);

        echo "Mail sent";
         //echo $email_body;
    }
}