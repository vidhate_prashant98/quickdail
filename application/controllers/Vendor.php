<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends CI_Controller {
	
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation','upload','email'));
        $this->load->helper(array('url','form','common_helper','text','string'));
        $this->load->model('VendorModel');
        $this->load->model('CommonModel');
        if($this->session->userdata('role') != 'VENDOR') {
            redirect(base_url().'Home/HomePage');
        }
    }
        
    public function logout() {
        $this->session->unset_userdata("vendor_username");
        $this->session->unset_userdata("vendor_role");
        redirect('Home/HomePage');
    }

    function viewProfile() {
        $username = $this->session->userdata('vendor_username'); 
        $data['profileDetail'] = $this->CommonModel->getProfile($username);
        $this->load->view('Vendor/Profile',$data);
    } 

    function updateVendor() {
        $name = htmlentities($this->security->xss_clean($_POST['vendorName']));
        $username = htmlentities($this->security->xss_clean($_POST['username']));
        $email = htmlentities($this->security->xss_clean($_POST['email']));
        $id = $this->VendorModel->updateVendor($name,$username,$email);
        if($id) {
            $this->session->set_flashdata('success', 'Your profile is updated successfully');
            redirect('Vendor/myAccount');
        } else {
            $this->session->set_flashdata('Error', 'Their is some problem in updation');
            redirect('Vendor/myAccount');
        }
    }

    function changePassword() {
        $this->load->view('admin/changepassword_view');
    }

    function updatePassword() {
        $oldpassword = sha1(htmlentities($this->security->xss_clean($this->input->post('oldpassword'))));
		$newpassword = sha1(htmlentities($this->security->xss_clean($this->input->post('newpassword'))));
        if($this->VendorModel->updatePassword($oldpassword,$newpassword)) {
            $this->session->set_flashdata('success', 'Password has been updated successfully. Please Login to continue');
            redirect('Vendor/changePassword');
        } else {
            $this->session->set_flashdata('error','Old password do not matched');
            redirect('Vendor/changePassword');	
        }	
    }

    function vendorList() {
        $username = $this->session->userdata('vendor_username');
        $data['vendorList'] = $this->VendorModel->getVendorListByUsername($username);
        $this->load->view('Vendor/Vendors',$data);
    }

    function viewAddVendor() {
        $username = $this->session->userdata('vendor_username');  // Display the vendor from only selected user
        $data['vendorList'] = $this->VendorModel->getVendorListByUsername($username);
        $this->load->view('Vendor/addVendor',$data);
    }

    function addVendor() {
        $name = $_POST['name'];
        $username = $_POST['username'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];

        $isOtherVendor = false;
        $parentVendorUsername = $this->session->userdata('vendor_username');  //select current logged in user as parent vendor

        if(isset($_POST['isOtherVendor'])){
            $isOtherVendor = true;
        }
        
        if($isOtherVendor){
            $parentVendorUsername = $_POST['selectedParentVendor'];     //select the parent vendor from UI
        }

        $vendorWidth = $this->VendorModel->getVendorWidth($parentVendorUsername)->levelcount;
        //$vendorHeight = $this->VendorModel->getVendorHeight($parentVendorUsername)->levelcount;
        
        if($vendorWidth < 10) {
            $result  = $this->VendorModel->AddVendor($name, $username, $email, $mobile, $parentVendorUsername);
        } else {
            echo 'Please select other vendor. The MAX LIMIT of vendor for this level is full.';
            die;
            $this->session->set_flashdata('error', 'Please select other vendor. The max limit of vendor for this level is full.');
            redirect('Vendor/viewAddVendor');
        }
    }
}
?>
